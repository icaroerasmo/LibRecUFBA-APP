package br.ufba.recsys.RecommendationService.other;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.HttpResponse;

public class Utils {
	public static String getParameter(String param, HttpServletRequest request) {
		String[] params = request.getParameterValues(param);
		if(params != null && params.length > 0) {
			return request.getParameterValues(param)[0];
		}
		return null;
	}
	
	public static String readHttpResponse(HttpResponse response) throws IOException {
		BufferedReader rd = new BufferedReader(
				new InputStreamReader(response.getEntity().getContent()));

		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
		return result.toString();
	}
}
