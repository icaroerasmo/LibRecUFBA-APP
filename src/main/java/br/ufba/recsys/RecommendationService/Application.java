package br.ufba.recsys.RecommendationService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;

import br.ufba.recsys.RecommendationService.models.PreferenceType;
import br.ufba.recsys.RecommendationService.models.User;
import br.ufba.recsys.RecommendationService.models.UserPreference;
import br.ufba.recsys.RecommendationService.repositories.PreferenceTypeRepository;
import br.ufba.recsys.RecommendationService.repositories.UserPreferenceRepository;
import br.ufba.recsys.RecommendationService.repositories.UserRepository;
import br.ufba.recsys.RecommendationService.services.PreferenceTypeService;
import br.ufba.recsys.RecommendationService.services.UserPreferenceService;
import br.ufba.recsys.RecommendationService.services.UserService;

@SpringBootApplication
public class Application {
	
	Logger logger = LoggerFactory.getLogger(Application.class);
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Order(1)
	@Bean
    CommandLineRunner init (UserService userService,
    		PreferenceTypeService preferenceTypeService,
    		UserPreferenceRepository userPreferenceRepository){
        return args -> {
        	
        	logger.info("Command Line Runner 1");
        	
        	User user = userService.getByUsername("admin");
        	
        	if(user == null) {
        		user = new User();
    			user.setUsername("admin");
    			user.setPassword("123456");
    			userService.save(user);
    		}
        	
        	User user1 = new User();
			user1.setUsername("icaroerasmo");
			user1.setPassword("123456");
			userService.save(user1);
        	
        	PreferenceType preferenceType1 = new PreferenceType();
        	preferenceType1.setName("Song");
        	preferenceTypeService.save(preferenceType1);
        	
        	PreferenceType preferenceType2 = new PreferenceType();
        	preferenceType2.setName("Book");
        	preferenceTypeService.save(preferenceType2);
        	
        	PreferenceType preferenceType3 = new PreferenceType();
        	preferenceType3.setName("Serie");
        	preferenceTypeService.save(preferenceType3);
        	
        	UserPreference userPreference1 = new UserPreference();
        	userPreference1.setName("Rock");
        	userPreference1.setPreferenceType(preferenceType1);
        	userPreference1.setUser(user);
        	userPreferenceRepository.save(userPreference1);
        	
        	UserPreference userPreference2 = new UserPreference();
        	userPreference2.setName("Harry Potter");
        	userPreference2.setPreferenceType(preferenceType2);
        	userPreference2.setUser(user);
        	userPreferenceRepository.save(userPreference2);
        };
    }
	
	@Order(2)
	@Bean
	CommandLineRunner init2 (UserRepository userRepository,
    		PreferenceTypeRepository preferenceTypeRepository,
    		UserPreferenceRepository userPreferenceRepository) {
		return args -> {
			
			logger.info("Command Line Runner 2");
			
			System.out.println("=============== USERS ========================");
			for(User user : userRepository.findAll()) {
				System.out.println("Id: "+user.getId());
				System.out.println("Username: "+user.getUsername());
				System.out.println("Password: "+user.getPassword()+"\n");
			}
			System.out.println("\n========== PREFERECE TYPES ==================");
			for(PreferenceType preferenceType : preferenceTypeRepository.findAll()) {
				System.out.println("Id: "+preferenceType.getId());
				System.out.println("Type: "+preferenceType.getName()+"\n");
			}
			System.out.println("\n========== USER PREFERENCES =================");
			for(UserPreference userPreference : userPreferenceRepository.findAll()) {
				System.out.println("Id: "+userPreference.getId());
				System.out.println("Name: "+userPreference.getName());
				System.out.println("User: "+userPreference.getUser().getUsername()+"\n");
			}
		};
	}
	
	@Bean
	CommandLineRunner init3(UserPreferenceService pService, UserService uService) {
		return args ->{
			System.out.println("============= PREFERECE TYPES ==================");
			System.out.println("================= BY USER ======================");
			User user = uService.getByUsername("admin");
			for(UserPreference userPreference : pService.getAllByUserId(user,
					user.getId())){
				System.out.println("Id: "+userPreference.getId());
				System.out.println("Name: "+userPreference.getName());
				System.out.println("User: "+userPreference.getUser().getUsername()+"\n");
			}
		};
	}
}

