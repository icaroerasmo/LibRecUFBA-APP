package br.ufba.recsys.RecommendationService.exceptions;

public class EntityNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8868958446638373106L;

	public EntityNotFoundException(String entityName, String id) {
		super(String.format("Entity of type '%s' with id '%s' not found",
				entityName, id));
	}
}
