package br.ufba.recsys.RecommendationService.exceptions;

public class BadRequestException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1349352261524592825L;

	public BadRequestException(String message) {
		super(message);
	}
}
