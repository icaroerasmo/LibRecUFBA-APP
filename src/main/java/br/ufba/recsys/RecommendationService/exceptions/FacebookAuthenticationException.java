package br.ufba.recsys.RecommendationService.exceptions;

public class FacebookAuthenticationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8708120290913808852L;
	
	public FacebookAuthenticationException() {
		super("Error during Facebook authentication");
	}

}
