package br.ufba.recsys.RecommendationService.exceptions;

public class WebResponseTypeNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4570720413187732202L;
	
	public WebResponseTypeNotFoundException(String status){
		super(String.format("%s not found", status));
	}

}
