package br.ufba.recsys.RecommendationService.exceptions;

public class NotSupportedFeatureException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2468389208277668038L;
	
	public NotSupportedFeatureException() {
		super("This feature is not supported");
	}
}
