package br.ufba.recsys.RecommendationService.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.ufba.recsys.RecommendationService.models.PreferenceType;

public interface PreferenceTypeRepository extends MongoRepository<PreferenceType, String> {

}
