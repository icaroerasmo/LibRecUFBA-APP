package br.ufba.recsys.RecommendationService.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.ufba.recsys.RecommendationService.models.User;
import br.ufba.recsys.RecommendationService.models.UserPreference;

public interface UserPreferenceRepository extends MongoRepository<UserPreference, String> {
	List<UserPreference> findAllByUser(User user);
}
