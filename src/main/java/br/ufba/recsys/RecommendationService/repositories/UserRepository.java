package br.ufba.recsys.RecommendationService.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.ufba.recsys.RecommendationService.models.User;

public interface UserRepository extends MongoRepository<User, String> {
	User findByUsername(String username);
}