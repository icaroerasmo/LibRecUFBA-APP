package br.ufba.recsys.RecommendationService.dto;

import java.util.Date;

public class FacebookClaims {
	private String  name;
	private String status;
	private Date exp;
	private AuthResponseDTO authResponse;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	public Date getExp() {
		return exp;
	}

	public void setExp(Date exp) {
		this.exp = exp;
	}

	/**
	 * @return the authResponse
	 */
	public AuthResponseDTO getAuthResponse() {
		return authResponse;
	}

	/**
	 * @param authResponse the authResponse to set
	 */
	public void setAuthResponse(AuthResponseDTO authResponse) {
		this.authResponse = authResponse;
	}
}
