package br.ufba.recsys.RecommendationService.dto;

import br.ufba.recsys.RecommendationService.enums.WebResponseTypesEnum;

public class WebResponseDTO {
	private WebResponseTypesEnum status;
	private String message;
	public WebResponseDTO(WebResponseTypesEnum status, String message) {
		super();
		this.status = status;
		this.message = message;
	}
	public String getStatus() {
		return status.getName();
	}
	public void setStatus(String status)
			throws Exception {
		this.status = WebResponseTypesEnum.get(status);
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
