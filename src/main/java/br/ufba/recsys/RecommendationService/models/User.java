package br.ufba.recsys.RecommendationService.models;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection="user")
public class User {
	  
	@Id
	@Field("_id")
	private ObjectId id;

	@Indexed(unique=true)
	private String username;
	
	private String name;
	private String password;

	public void setId(ObjectId _id) {
	 this.id = _id;
	}

	public String getId() {
	 return id != null ? this.id.toHexString() : null;
	}

	public String getUsername() {
		return username;
	}

	public void setPassword(String password) {
	 this.password = password;
	}

	public String getPassword() {
	 return password;
	}

	public void setUsername(String username) {
	 this.username = username;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
}