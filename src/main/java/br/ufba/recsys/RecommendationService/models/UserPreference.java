package br.ufba.recsys.RecommendationService.models;

import javax.validation.constraints.NotNull;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection="user_preferences")
public class UserPreference {
	
	@Id
	@Field("_id")
	private ObjectId id;
	
	@NotNull
	private String name;
	
	@DBRef
	@NotNull
	private PreferenceType preferenceType;
	
	@DBRef
	@NotNull
	private User user;
	
	public String getId() {
		return id != null ? id.toHexString() : null;
	}
	public void setId(ObjectId id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public PreferenceType getPreferenceType() {
		return preferenceType;
	}
	public void setPreferenceType(PreferenceType preferenceType) {
		this.preferenceType = preferenceType;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
}
