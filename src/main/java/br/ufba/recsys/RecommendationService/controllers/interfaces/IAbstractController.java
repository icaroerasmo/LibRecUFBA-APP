package br.ufba.recsys.RecommendationService.controllers.interfaces;

import java.util.List;

import br.ufba.recsys.RecommendationService.dto.WebResponseDTO;

public interface IAbstractController<T> {
	WebResponseDTO save(T t) throws Exception;
	WebResponseDTO delete(String id) throws Exception;
	List<T> getAll() throws Exception;
	T getById(T t, String id) throws Exception;
}
