package br.ufba.recsys.RecommendationService.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import br.ufba.recsys.RecommendationService.controllers.interfaces.IAbstractController;
import br.ufba.recsys.RecommendationService.dto.WebResponseDTO;
import br.ufba.recsys.RecommendationService.services.interfaces.IAbstractService;

public class AbstractController<T> implements IAbstractController<T> {

	@Autowired
	protected IAbstractService<T> service;
	
	@Override
	@PostMapping
	public WebResponseDTO save(@RequestBody T t) throws Exception {
		return service.save(t);
	}

	@Override
	@DeleteMapping("/{id}")
	public WebResponseDTO delete(@PathVariable String id) 
			throws Exception {
		return service.delete(id);
	}

	@Override
	@GetMapping
	public List<T> getAll() throws Exception {
		return service.getAll();
	}

	@Override
	@GetMapping("/{id}")
	public T getById(@PathVariable("id") T t,
			@PathVariable("id")String id) throws Exception {
		return service.getById(t, id);
	}
}
