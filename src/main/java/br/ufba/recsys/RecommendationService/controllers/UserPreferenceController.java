package br.ufba.recsys.RecommendationService.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.ufba.recsys.RecommendationService.dto.WebResponseDTO;
import br.ufba.recsys.RecommendationService.models.User;
import br.ufba.recsys.RecommendationService.models.UserPreference;
import br.ufba.recsys.RecommendationService.services.UserPreferenceService;

@RestController
@RequestMapping("/userPreference")
public class UserPreferenceController {
	
	@Autowired
	private UserPreferenceService service;
	
	@GetMapping("/user/{userId}")
	public List<UserPreference> getUserPreferencesByUserId(
			@PathVariable("userId") User user,
			@PathVariable("userId") String id) throws Exception{
		return service.getAllByUserId(user, id);
	}
	
	@PostMapping("/user/{userId}")
	public WebResponseDTO mergeUserPreferences(
			@PathVariable("userId") User user, @PathVariable("userId") String id,
			@RequestBody List<UserPreference> preferences)
					throws Exception {
		return service.mergeUserPreferences(user, id, preferences);
	}
}
