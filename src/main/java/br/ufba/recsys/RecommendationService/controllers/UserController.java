package br.ufba.recsys.RecommendationService.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.ufba.recsys.RecommendationService.config.FacebookAuthenticationToken;
import br.ufba.recsys.RecommendationService.dto.WebResponseDTO;
import br.ufba.recsys.RecommendationService.models.User;
import br.ufba.recsys.RecommendationService.services.UserService;

@RestController
@RequestMapping("/user")
public class UserController extends AbstractController<User>{
	@Autowired
	private UserService service;
	
	@PostMapping("/password")
	public WebResponseDTO updatePassword(@RequestBody User user)
			throws Exception {
		return service.updatePassword(user);
	}
	
	@GetMapping("/auth")
	public Authentication getAuthentication() {
		return SecurityContextHolder.
				getContext().getAuthentication();
	}
	
	@GetMapping("facebookName")
	public String  getName() {
		FacebookAuthenticationToken  auth = 
				(FacebookAuthenticationToken) SecurityContextHolder.
					getContext().getAuthentication();
		return auth.getName();
	}
}
