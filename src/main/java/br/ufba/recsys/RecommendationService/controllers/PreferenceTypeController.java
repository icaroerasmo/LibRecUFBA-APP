package br.ufba.recsys.RecommendationService.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.ufba.recsys.RecommendationService.models.PreferenceType;

@RestController
@RequestMapping("/preferenceType")
public class PreferenceTypeController extends AbstractController<PreferenceType>{

}
