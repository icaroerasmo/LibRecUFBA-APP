package br.ufba.recsys.RecommendationService.controllers;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import br.ufba.recsys.RecommendationService.dto.AuthResponseDTO;
import br.ufba.recsys.RecommendationService.dto.FacebookAccessTokenDTO;
import br.ufba.recsys.RecommendationService.dto.FacebookClaims;
import br.ufba.recsys.RecommendationService.exceptions.FacebookAuthenticationException;
import br.ufba.recsys.RecommendationService.other.Utils;

@RestController
@RequestMapping("/facebook")
public class FacebookConnectionController {
	
	private Map<String, String> activeLogins;  
	
	private static final String APP_FACEBOOK_LOGIN = "http://localhost:8080/facebook/login";
	private static final String FACEBOOK_CALLBACK = "http://localhost:8080/facebook/connect";
	private static final String FACEBOOK_AUTH_URL = "https://graph.facebook.com/v3.2/oauth/access_token?" +
					"client_id=%s" +
					"&client_secret=%s" +
					"&code=%s" +
					"&redirect_uri=%s";
	private static final String FACEBOOK_CODE_URL = "https://www.facebook.com/v3.2/dialog/oauth?" + 
			"client_id=%s" + 
			"&redirect_uri=%s" + 
			"&state=%s";
	
	@Value("${spring.social.facebook.appId}")
	private String appId;
	
	@Value("${spring.social.facebook.appSecret}")
	private String appSecret;
	
	@PostConstruct
	private void init() {
		this.activeLogins = new HashMap<>();
	}
	
	@GetMapping("/request")
	public String getLink(@RequestParam(defaultValue = "button") String mode, @RequestParam String uuid) throws Exception {
		String path = String.format(FACEBOOK_CODE_URL, appId, FACEBOOK_CALLBACK, uuid);
		if(mode.equals("link")) {
			return path;
		} else {
			return String.format("<a href=\"%s\">Connect</a>", path);
		}
	}
	
	@GetMapping("connect")
	public void connect(@RequestParam String code, @RequestParam String state) throws Exception {
		FacebookAccessTokenDTO accessToken = getAccessToken(code);
		FacebookClaims claims = new FacebookClaims();
		claims.setStatus("connected");
		AuthResponseDTO auth = new AuthResponseDTO();
		auth.setAccessToken(accessToken.getAccess_token());
		auth.setExpiresIn(new Long(accessToken.getExpires_in()));
		claims.setAuthResponse(auth);
		HttpClient client = HttpClientBuilder.create().build();
		HttpPost tokenPost = new HttpPost(APP_FACEBOOK_LOGIN);
		Gson g = new Gson();
		tokenPost.setEntity(new StringEntity(g.toJson(claims)));
		tokenPost.setHeader("Content-Type", "application/json");
		HttpResponse response = client.execute(tokenPost);
		Header authorizationHeader = response.getFirstHeader("Authorization");
		
		if(authorizationHeader == null) {
			throw new FacebookAuthenticationException();
		}
		
		activeLogins.put(state, authorizationHeader.getValue());
	}
	
	@GetMapping("facebookToken")
	private String returnToken(@RequestParam String uuid) {
		return activeLogins.remove(uuid);
	}
	
	private FacebookAccessTokenDTO getAccessToken(String code) {
		String path = String.format(
				FACEBOOK_AUTH_URL, appId, appSecret,
				code, FACEBOOK_CALLBACK);
		
		HttpClient client = HttpClientBuilder.create().build();
		HttpGet tokenGet = new HttpGet(path);
		try {
			HttpResponse response = client.execute(tokenGet);
			if(response.getStatusLine().getStatusCode() == 200) {
				Gson g = new Gson();
				FacebookAccessTokenDTO fb = g.fromJson(
						Utils.readHttpResponse(response),
						FacebookAccessTokenDTO.class);
				return fb;	
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
