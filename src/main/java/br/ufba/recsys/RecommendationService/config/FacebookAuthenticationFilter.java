package br.ufba.recsys.RecommendationService.config;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.ufba.recsys.RecommendationService.dto.FacebookClaims;
import br.ufba.recsys.RecommendationService.models.User;
import br.ufba.recsys.RecommendationService.repositories.UserRepository;

@Service
@Configurable
public class FacebookAuthenticationFilter extends AbstractAuthenticationProcessingFilter{

	@Autowired
	private UserRepository repository;
	
	@Value("${spring.social.facebook.appId}")
	private String appId;
	
	@Value("${spring.social.facebook.appSecret}")
	private String appSecret;
	
	protected FacebookAuthenticationFilter() {
		super(new AntPathRequestMatcher("/facebook/login", "POST"));
		setAuthenticationManager(
				new FacebookAuthenticationManager());
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request,
			HttpServletResponse response)
			throws AuthenticationException, IOException, ServletException {

		FacebookClaims facebookPrincipal = new ObjectMapper()
				.readValue(request.getInputStream(), FacebookClaims.class);
		
		return getAuthenticationManager().authenticate(
				new FacebookAuthenticationToken(facebookPrincipal));
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication auth) throws IOException, ServletException {
		
		FacebookAuthenticationToken fToken =
				(FacebookAuthenticationToken) auth;
		
		User user = repository.findByUsername(
				((FacebookClaims)fToken.getClaims()).
				getAuthResponse().getUserID()); 
		
		if(user == null) {
			user = new User();
			user.setUsername(fToken.getPrincipal());
			user.setName(fToken.getName());
		}
		
		repository.save(user);
		
		SecurityContextHolder.getContext().setAuthentication(fToken);
		TokenAuthenticationService.addFacebookAuthentication(response, fToken.getClaims());
	}
}
