package br.ufba.recsys.RecommendationService.config;

import java.util.Arrays;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import br.ufba.recsys.RecommendationService.dto.FacebookClaims;

public class FacebookAuthenticationToken extends AbstractAuthenticationToken {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4939347301477373898L;

	private FacebookClaims claims;
	
	public FacebookAuthenticationToken(FacebookClaims claims) {
		super(Arrays.asList(new SimpleGrantedAuthority("FACEBOOK_USER")));
		this.claims = claims;
	}
	
	public FacebookClaims getClaims() {
		return this.claims;
	}

	@Override
	public String getCredentials() {
		return claims != null ? 
				claims.getAuthResponse().getAccessToken():null;
	}
	
	@Override
	public String getPrincipal() {
		return claims != null ? 
				claims.getAuthResponse().getUserID():null;
	}
	
	public void setPrincipal(String principal) {
		if(claims != null) {
			claims.getAuthResponse().setUserID(principal);
		}
	}
}
