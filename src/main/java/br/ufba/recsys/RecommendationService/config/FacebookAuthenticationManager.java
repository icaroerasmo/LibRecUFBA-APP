package br.ufba.recsys.RecommendationService.config;

import java.io.IOException;
import java.util.Date;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import br.ufba.recsys.RecommendationService.dto.FacebookClaims;
import br.ufba.recsys.RecommendationService.dto.FacebookUserInfoDTO;
import br.ufba.recsys.RecommendationService.other.Utils;

public class FacebookAuthenticationManager implements AuthenticationManager{

	private static final String FACEBOOK_USER_INFO_URL = "https://graph.facebook.com/me";

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		FacebookAuthenticationToken facebookAuthenticationToken = (FacebookAuthenticationToken)authentication;
		getUserInfo(facebookAuthenticationToken);
		return facebookAuthenticationToken;
	}
	
	private void getUserInfo(FacebookAuthenticationToken facebookAuthenticationToken) {
		String token =
				((FacebookClaims)facebookAuthenticationToken
						.getClaims())
				.getAuthResponse().getAccessToken();
		String path = String.format("%s?access_token=%s",
				FACEBOOK_USER_INFO_URL, token);
		HttpClient client = HttpClientBuilder.create().build();
		HttpGet tokenGet = new HttpGet(path);
		try {
			HttpResponse response = client.execute(tokenGet);
			if(response.getStatusLine().getStatusCode() == 200) {
				Gson g = new Gson();
				
				FacebookUserInfoDTO userInfo = 
						g.fromJson(Utils.readHttpResponse(response), FacebookUserInfoDTO.class);
				
				facebookAuthenticationToken.getClaims().setName(userInfo.getName());
				
				if(facebookAuthenticationToken.getPrincipal() == null) {
					facebookAuthenticationToken.setPrincipal(userInfo.getId());
				}
				if(facebookAuthenticationToken.getClaims().getExp() == null) {
					long expiresIn = facebookAuthenticationToken.getClaims().
							getAuthResponse().getExpiresIn() + System.currentTimeMillis();
					facebookAuthenticationToken.getClaims().setExp(new Date(expiresIn));
				}
				facebookAuthenticationToken.setAuthenticated(true);
			} else {
				facebookAuthenticationToken.setAuthenticated(false);
			}
		} catch (JsonSyntaxException | IOException e) {
			e.printStackTrace();
		}
	}
}
