package br.ufba.recsys.RecommendationService.config;

import java.util.Collections;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializer;

import br.ufba.recsys.RecommendationService.dto.AuthResponseDTO;
import br.ufba.recsys.RecommendationService.dto.FacebookClaims;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class TokenAuthenticationService {
	
	// EXPIRATION_TIME = 10 dias
	static final long EXPIRATION_TIME = 860_000_000;
	static final String SECRET = "LibRecUFBA";
	static final String TOKEN_PREFIX = "Bearer";
	static final String HEADER_STRING = "Authorization";
	
	
	static void addAuthentication(HttpServletResponse response, String username) {
		String JWT = getToken(username, EXPIRATION_TIME);
		response.addHeader(HEADER_STRING, TOKEN_PREFIX + " " + JWT);
	}
	
	static void addFacebookAuthentication(HttpServletResponse response, FacebookClaims claims) {
		Gson g = new GsonBuilder()
		        .registerTypeAdapter(Date.class, (JsonSerializer<Date>)
		        		(date, type, jsonSerializationContext) -> new JsonPrimitive(date.getTime()))
		        .create();
		String JWT = getFacebookToken(g.toJson(claims));
		response.addHeader(HEADER_STRING, TOKEN_PREFIX + " " + JWT);
	}

	private static String getToken(String username, long expirationTime) {
		String JWT = Jwts.builder()
					.setSubject(username)
					.setExpiration(new Date(System.currentTimeMillis() + expirationTime))
					.signWith(SignatureAlgorithm.HS512, SECRET)
					.compact();
		
		return JWT;
	}
	
	private static String getFacebookToken(String payload) {
		String JWT = Jwts.builder()
					.setPayload(payload)
					.signWith(SignatureAlgorithm.HS512, SECRET)
					.compact();
		
		return JWT;
	}
	
	static Authentication getAuthentication(HttpServletRequest request) {
		String token = request.getHeader(HEADER_STRING);
		
		if (token != null) {
			String user;
			
			Claims clms = Jwts.parser()
			.setSigningKey(SECRET)
			.parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
			.getBody();
			
			if(!StringUtils.isEmpty(clms.getSubject())) {
				user = clms.getSubject();
				if (user != null) {
					return new UsernamePasswordAuthenticationToken(user, null, Collections.emptyList());
				}
			}
			FacebookAuthenticationToken fat = new FacebookAuthenticationToken(parseClaims(clms));
			fat.setAuthenticated(true);
			return fat;
		}
		return null;
	}
	
	private static FacebookClaims parseClaims(Map<String, Object> claims) {
		FacebookClaims fp = new FacebookClaims();
		fp.setName((String)claims.get("name"));
		fp.setStatus((String)claims.get("status"));
		
		@SuppressWarnings("unchecked")
		Map<String, Object> auth = (Map<String, Object>) claims.get("authResponse");
		
		AuthResponseDTO authR = new AuthResponseDTO();
		authR.setUserID((String)auth.get("userID"));
		authR.setAccessToken((String)auth.get("accessToken"));
		authR.setExpiresIn(new Long((Integer)auth.get("expiresIn")));
		authR.setSecret((String) auth.get("secret"));
		authR.setSig((String)auth.get("sig"));
		fp.setAuthResponse(authR);
		return fp;
	}
}
