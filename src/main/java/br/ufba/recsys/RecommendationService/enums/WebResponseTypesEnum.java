package br.ufba.recsys.RecommendationService.enums;

import br.ufba.recsys.RecommendationService.exceptions.WebResponseTypeNotFoundException;

public enum WebResponseTypesEnum {
	DANGER, INFO, SUCCESS, WARNING;
	
	public String getName() {
		return this.toString().toLowerCase();
	}

	public static WebResponseTypesEnum get(String status)
			throws Exception {
		for(WebResponseTypesEnum w : WebResponseTypesEnum.values()) {
			if(status.equalsIgnoreCase(w.toString())) {
				return w;
			}
		}
		throw new WebResponseTypeNotFoundException(status);
	}
}
