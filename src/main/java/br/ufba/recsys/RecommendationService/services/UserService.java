package br.ufba.recsys.RecommendationService.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import br.ufba.recsys.RecommendationService.dto.WebResponseDTO;
import br.ufba.recsys.RecommendationService.exceptions.BadRequestException;
import br.ufba.recsys.RecommendationService.exceptions.EntityNotFoundException;
import br.ufba.recsys.RecommendationService.exceptions.NotSupportedFeatureException;
import br.ufba.recsys.RecommendationService.models.User;
import br.ufba.recsys.RecommendationService.repositories.UserRepository;

@Service
public class UserService extends AbstractService<User>{
	
	@Autowired
	private UserRepository repository;
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Override
	public WebResponseDTO save(User t) throws Exception {
		if(t.getId() != null) {
			Optional<User> user = repository.findById(t.getId());
			if(!user.isPresent()) {
				t.setPassword(user.get().getPassword());
				return super.save(t);
			}
		}
		t.setPassword(bCryptPasswordEncoder.
				encode(t.getPassword()));
		return super.save(t);
	}
	
	public WebResponseDTO updatePassword(User u) throws Exception,
			NotSupportedFeatureException {
		if(u.getId() != null &&
				u.getPassword() != null) {
			Optional<User> user = repository.findById(u.getId());
			if(user.isPresent()) {
				User _user = user.get();
				_user.setPassword(bCryptPasswordEncoder.
						encode(u.getPassword()));
				
				WebResponseDTO response = super.save(_user);
				response.setMessage("Password updated successfully");
				
				return response;
			}
			throw new EntityNotFoundException(getEntityName(), u.getId());
		}
		throw new BadRequestException("There was an error executing this request");
	}

	public User getByUsername(String username) {
		return repository.findByUsername(username);
	}
}
