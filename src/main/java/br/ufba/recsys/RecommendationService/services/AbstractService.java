package br.ufba.recsys.RecommendationService.services;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.GenericTypeResolver;
import org.springframework.data.mongodb.repository.MongoRepository;

import br.ufba.recsys.RecommendationService.dto.WebResponseDTO;
import br.ufba.recsys.RecommendationService.enums.WebResponseTypesEnum;
import br.ufba.recsys.RecommendationService.exceptions.EntityNotFoundException;
import br.ufba.recsys.RecommendationService.services.interfaces.IAbstractService;

public class AbstractService<T> implements IAbstractService<T>{

	private Class<?> type;
	
	@SuppressWarnings("unchecked")
	public AbstractService() {
		this.type = (Class<T>) GenericTypeResolver.
				resolveTypeArgument(getClass(), AbstractService.class);
	}
	
	private static final String REGEX =
			"(?<!(^|[A-Z]))(?=[A-Z])|(?<!^)(?=[A-Z][a-z])";
	
	@Autowired
	protected MongoRepository<T, String> repository;
	
	@Override
	public WebResponseDTO save(T t) throws Exception {
		repository.save(t);
		return new WebResponseDTO(WebResponseTypesEnum.SUCCESS,
				String.format("%s saved successfully",
						getEntityName()));
	}

	@Override
	public WebResponseDTO delete(String id) throws Exception {
		Optional<T> t = repository.findById(id);
		if(t.isPresent()) {
			repository.delete(t.get());
			return new WebResponseDTO(WebResponseTypesEnum.SUCCESS,
					String.format("%s with id %s deleted successfully",
							getEntityName(), id)
					);
		}
		throw new EntityNotFoundException(getEntityName(), id);
	}

	@Override
	public List<T> getAll() throws Exception {
		return repository.findAll();
	}

	@Override
	public T getById(T t, String id) throws Exception {
		if(t != null) {
			return t;
		}
		throw new EntityNotFoundException(getEntityName(), id);
	}

	public String getEntityName() {
		return StringUtils.join(type.getSimpleName().split(REGEX), " ");
	}
}
