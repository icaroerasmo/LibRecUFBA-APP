package br.ufba.recsys.RecommendationService.services.interfaces;

import java.util.List;

import br.ufba.recsys.RecommendationService.dto.WebResponseDTO;

public interface IAbstractService<T> {
	WebResponseDTO save(T t) throws Exception;
	WebResponseDTO delete(String id) throws Exception;
	List<T> getAll() throws Exception;
	T getById(T t, String id) throws Exception;
}
