package br.ufba.recsys.RecommendationService.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.ufba.recsys.RecommendationService.dto.WebResponseDTO;
import br.ufba.recsys.RecommendationService.enums.WebResponseTypesEnum;
import br.ufba.recsys.RecommendationService.exceptions.EntityNotFoundException;
import br.ufba.recsys.RecommendationService.exceptions.NotSupportedFeatureException;
import br.ufba.recsys.RecommendationService.models.User;
import br.ufba.recsys.RecommendationService.models.UserPreference;
import br.ufba.recsys.RecommendationService.repositories.UserPreferenceRepository;

@Service
public class UserPreferenceService extends AbstractService<UserPreference> {

	@Autowired
	private UserPreferenceRepository repository;

	public WebResponseDTO mergeUserPreferences
	(User user, String id, List<UserPreference> toSave)
			throws Exception {
		if(user != null) {
			List<UserPreference> toDelete = 
					repository.findAllByUser(user);
			
			toDelete.removeIf(d -> toSave.stream().anyMatch(
					s -> d.getId().equals(s.getId())));
			
			repository.deleteAll(toDelete);
			
			toSave.forEach(up -> up.setUser(user));
			
			repository.saveAll(toSave);
	
			return new WebResponseDTO(WebResponseTypesEnum.SUCCESS,
					String.format("%s collection saved successfully",
							getEntityName()));
		}
		throw new EntityNotFoundException("User", id);
	}
	
	public List<UserPreference> getAllByUserId(User user, String id)
			throws Exception{
		if(user != null) {
			return repository.findAllByUser(user);
		}
		throw new EntityNotFoundException("User", id);
	} 
	
	@Override
	public WebResponseDTO save(UserPreference t) throws Exception {
		throw new NotSupportedFeatureException();
	}

	@Override
	public List<UserPreference> getAll() throws Exception {
		throw new NotSupportedFeatureException();
	}

	@Override
	public WebResponseDTO delete(String id) throws Exception,
	NotSupportedFeatureException {
		throw new NotSupportedFeatureException();
	}
	
	@Override
	public UserPreference getById(UserPreference t, String id) throws Exception {
		throw new NotSupportedFeatureException();
	}
}
