package br.ufba.recsys.RecommendationService.services;

import org.springframework.stereotype.Service;

import br.ufba.recsys.RecommendationService.models.PreferenceType;

@Service
public class PreferenceTypeService extends AbstractService<PreferenceType>{

}
